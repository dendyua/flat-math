
#pragma once

#include <cstdint>

#include "Debug.hpp"




#ifdef FLAT_MATH_USE_EXCEPTIONS
#	define FLAT_MATH_CHECK(cond) \
		FLAT_CHECK(cond) << "Overflow"
#else
#	define FLAT_MATH_CHECK(cond) \
		FLAT_ASSERT(cond) << "Overflow"
#endif




namespace Flat {
namespace Math {




static constexpr int32_t  kInt32Min  = int32_t(-2147483648);
static constexpr int32_t  kInt32Max  = int32_t( 2147483647);
static constexpr uint32_t kUint32Max = uint32_t(4294967295U);

static constexpr int64_t kInt64Min   = int64_t(-9223372036854775807LL) - 1;
static constexpr int64_t kInt64Max   = int64_t( 9223372036854775807LL);




template <typename T> constexpr int sign(T a);

template <typename T> FLAT_MATH_EXPORT T add_int_safe(T a, T b);
template <typename T> FLAT_MATH_EXPORT T sub_int_safe(T a, T b);
template <typename T> FLAT_MATH_EXPORT void inc_int_safe(T & a);
template <typename T> FLAT_MATH_EXPORT void dec_int_safe(T & a);
template <typename T> FLAT_MATH_EXPORT T minus_int_safe(T a);
template <typename T> FLAT_MATH_EXPORT T mul_int_safe(T a, T b);
template <typename T> FLAT_MATH_EXPORT T div_int_safe(T a, T b);
template <typename T> FLAT_MATH_EXPORT T mod_int_safe(T a, T b);

FLAT_MATH_EXPORT int32_t mul_real_safe_32(int32_t a, int32_t b);
FLAT_MATH_EXPORT int64_t mul_real_safe_64(int64_t a, int64_t b);

FLAT_MATH_EXPORT int32_t div_real_safe_32(int32_t a, int32_t b);
FLAT_MATH_EXPORT int64_t div_real_safe_64(int64_t a, int64_t b);

template <typename T> T add_int(T a, T b);
template <typename T> T sub_int(T a, T b );
template <typename T> void inc_int(T & a);
template <typename T> void dec_int(T & a);
template <typename T> T minus_int(T a);
template <typename T> T mul_int(T a, T b);
template <typename T> T div_int(T a, T b);
template <typename T> T mod_int(T a, T b);

template <typename T> T mul_real(T a, T b);
template <typename T> T div_real(T a, T b);




template <typename T>
inline constexpr int sign(const T a)
{ return a < T(0) ? -1 : 1; }


template <typename T>
inline T add_int(const T a, const T b)
{
#ifdef FLAT_MATH_USE_SAFE_ARITHMETIC
	return add_int_safe(a, b);
#else
	return a + b;
#endif
}


template <typename T>
inline T sub_int( const T a, const T b )
{
#ifdef FLAT_MATH_USE_EXCEPTIONS
	return sub_int_safe(a, b);
#else
	return a - b;
#endif
}


template <typename T>
inline void inc_int(T & a)
{
#ifdef FLAT_MATH_USE_EXCEPTIONS
	inc_int_safe(a);
#else
	a++;
#endif
}


template <typename T>
inline void dec_int(T & a)
{
#ifdef FLAT_MATH_USE_EXCEPTIONS
	dec_int_safe(a);
#else
	a--;
#endif
}


template <typename T>
inline T minus_int(const T a)
{
#ifdef FLAT_MATH_USE_EXCEPTIONS
	return minus_int_safe(a);
#else
	return -a;
#endif
}


template <typename T>
inline T mul_int(const T a, const T b)
{
#ifdef FLAT_MATH_USE_EXCEPTIONS
	return mul_int_safe(a, b);
#else
	return a * b;
#endif
}


template <typename T>
inline T div_int(const T a, const T b)
{
#ifdef FLAT_MATH_USE_EXCEPTIONS
	return div_int_safe(a, b);
#else
	return a / b;
#endif
}


template <typename T>
inline T mod_int(const T a, const T b)
{
#ifdef FLAT_MATH_USE_EXCEPTIONS
	return mod_int_safe(a, b);
#else
	return a % b;
#endif
}


template <>
inline int32_t mul_real<int32_t>(const int32_t a, const int32_t b)
{
#ifdef FLAT_MATH_USE_EXCEPTIONS
	return mul_real_safe_32(a, b);
#else
	return static_cast<int32_t>((static_cast<int64_t>(a)*b) >> 32);
#endif
}


template <>
inline int64_t mul_real<int64_t>(const int64_t a, const int64_t b)
{
#ifdef FLAT_MATH_USE_EXCEPTIONS
	return mul_real_safe_64(a, b);
#else
	return a * b;
#endif
}


template <typename T>
inline T div_real(const T a, const T b)
{
	return 0;
}




}}
