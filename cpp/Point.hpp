
#pragma once

#include "Global.h"

#include "Int.h"
#include "Real.h"




namespace Grim {
namespace Math {




template <typename T>
class _Point
{
public:
	_Point();
	_Point( const T & x, const T & y );

	const T & x() const;
	const T & y() const;

	bool operator==( const _Point<T> & other ) const;
	bool operator!=( const _Point<T> & other ) const;

	T & rx();
	T & ry();

	_Point<T> & operator+=( const _Point<T> & other );

	template <typename K> friend inline _Point<K> operator+( const _Point<K> & a, const _Point<K> & b );

private:
	T x_;
	T y_;
};




typedef _Point<Int32> Point32;
typedef _Point<Int64> Point64;

typedef _Point<Real32> RealPoint32;




template <typename T>
inline _Point<T>::_Point()
{}

template <typename T>
inline _Point<T>::_Point( const T & x, const T & y ) :
	x_( x ), y_( y )
{}

template <typename T>
inline const T & _Point<T>::x() const
{ return x_; }

template <typename T>
inline const T & _Point<T>::y() const
{ return y_; }

template <typename T>
inline bool _Point<T>::operator==( const _Point<T> & other ) const
{ return x_ == other.x_ && y_ == other.y_; }

template <typename T>
inline bool _Point<T>::operator!=( const _Point<T> & other ) const
{ return x_ != other.x_ || y_ != other.y_; }

template <typename T>
inline T & _Point<T>::rx()
{ return x_; }

template <typename T>
inline T & _Point<T>::ry()
{ return y_; }

template <typename T>
inline _Point<T> & _Point<T>::operator+=( const _Point<T> & other )
{ x_ += other.x(); y_ += other.y(); return *this; }




template <typename K>
inline _Point<K> operator+( const _Point<K> & a, const _Point<K> & b )
{ return _Point<K>( a.x() + b.x(), a.y() + b.y() ); }




} // namespace Math
} // namespace Grim




namespace Grim {
namespace Debug {

template <typename T>
inline void operator<<( Log::Append & append, const Grim::Math::_Point<T> & point )
{
	Log::State state( append.log() );
	FLAT_UNUSED(state)

	append.log().maybeSpace();
	append.log() << Log::NoSpace();

	append.log() << "Point(" << point.x() << ", " << point.y() << ")";
}

} // namespace Debug
} // namespace Grim
