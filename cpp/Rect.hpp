
#pragma once

#include "Global.h"
#include "Point.h"
#include "Size.h"




namespace Grim {
namespace Math {




template <typename T>
class _Rect
{
public:
	_Rect();
	_Rect( const _Point<T> & bottomLeft, const _Point<T> & topRight );
	_Rect( const T & x, const T & y, const T & width, const T & height );
	_Rect( const _Point<T> & bottomLeft, const _Size<T> & size );

	bool isNull() const;
	bool isEmpty() const;

	T x() const;
	T y() const;
	T width() const;
	T height() const;

	_Size<T> size() const;

	T left() const;
	T right() const;
	T bottom() const;
	T top() const;

	_Point<T> bottomLeft() const;
	_Point<T> topRight() const;
	_Point<T> bottomRight() const;
	_Point<T> topLeft() const;

	bool contains( const T & x, const T & y ) const;
	bool contains( const _Point<T> & pos ) const;

	_Rect<T> adjusted( const T & left, const T & bottom, const T & top, const T & right ) const;

	bool operator==( const _Rect<T> & other ) const;
	bool operator!=( const _Rect<T> & other ) const;

	void setLeft( const T & left );
	void setRight( const T & right );
	void setBottom( const T & bottom );
	void setTop( const T & top );

private:
	_Point<T> bottomLeft_;
	_Point<T> topRight_;
};




typedef _Rect<Int32> Rect32;
typedef _Rect<Int64> Rect64;

typedef _Rect<Real32> RealRect32;




template <typename T>
inline _Rect<T>::_Rect() :
	bottomLeft_(),
	topRight_( -T::one(), -T::one() )
{}

template <typename T>
inline _Rect<T>::_Rect( const _Point<T> & bottomLeft, const _Point<T> & topRight ) :
	bottomLeft_( bottomLeft ),
	topRight_( topRight )
{}

template <typename T>
inline _Rect<T>::_Rect( const T & x, const T & y, const T & width, const T & height ) :
	bottomLeft_( x, y ),
	topRight_( x + width, y + height )
{}

template <typename T>
inline _Rect<T>::_Rect( const _Point<T> & bottomLeft, const _Size<T> & size ) :
	bottomLeft_( bottomLeft ),
	topRight_( bottomLeft.x() + size.width(), bottomLeft.y() + size.height() )
{}

template <typename T>
inline bool _Rect<T>::isNull() const
{ return *this == _Rect<T>(); }

template <typename T>
inline bool _Rect<T>::isEmpty() const
{ return width() <= T() || height() <= T(); }

template <typename T>
inline T _Rect<T>::x() const
{ return bottomLeft_.x(); }

template <typename T>
inline T _Rect<T>::y() const
{ return bottomLeft_.y(); }

template <typename T>
inline T _Rect<T>::width() const
{ return topRight_.x() - bottomLeft_.x(); }

template <typename T>
inline T _Rect<T>::height() const
{ return topRight_.y() - bottomLeft_.y(); }

template <typename T>
inline _Size<T> _Rect<T>::size() const
{ return _Size<T>( width(), height() ); }

template <typename T>
inline T _Rect<T>::left() const
{ return bottomLeft_.x(); }

template <typename T>
inline T _Rect<T>::right() const
{ return topRight_.x(); }

template <typename T>
inline T _Rect<T>::bottom() const
{ return bottomLeft_.y(); }

template <typename T>
inline T _Rect<T>::top() const
{ return topRight_.y(); }

template <typename T>
inline _Point<T> _Rect<T>::bottomLeft() const
{ return bottomLeft_; }

template <typename T>
inline _Point<T> _Rect<T>::topRight() const
{ return topRight_; }

template <typename T>
inline _Point<T> _Rect<T>::bottomRight() const
{ return _Point<T>( topRight_.x(), bottomLeft_.y() ); }

template <typename T>
inline _Point<T> _Rect<T>::topLeft() const
{ return _Point<T>( bottomLeft_.x(), topRight_.y() ); }

template <typename T>
inline bool _Rect<T>::contains( const T & x, const T & y ) const
{ return x >= bottomLeft_.x() && x <= topRight_.x() && y >= bottomLeft_.y() && y <= topRight_.y(); }

template <typename T>
inline bool _Rect<T>::contains( const _Point<T> & pos ) const
{ return contains( pos.x(), pos.y() ); }

template <typename T>
inline _Rect<T> _Rect<T>::adjusted( const T & left, const T & bottom, const T & top, const T & right ) const
{ return _Rect<T>( bottomLeft_ + _Point<T>( left, bottom ), topRight_ + _Point<T>( top, right ) ); }

template <typename T>
inline bool _Rect<T>::operator==( const _Rect<T> & other ) const
{ return bottomLeft_ == other.bottomLeft_ && topRight_ == other.topRight_; }

template <typename T>
inline bool _Rect<T>::operator!=( const _Rect<T> & other ) const
{ return bottomLeft_ != other.bottomLeft_ || topRight_ != other.topRight_; }

template <typename T>
inline void _Rect<T>::setLeft( const T & left )
{ bottomLeft_.rx() = left; }

template <typename T>
inline void _Rect<T>::setRight( const T & right )
{ topRight_.rx() = right; }

template <typename T>
inline void _Rect<T>::setBottom( const T & bottom )
{ bottomLeft_.ry() = bottom; }

template <typename T>
inline void _Rect<T>::setTop( const T & top )
{ topRight_.ry() = top; }




} // namespace Math
} // namespace Grim





namespace Grim {
namespace Debug {

template <typename T>
inline void operator<<( Log::Append & append, const Grim::Math::_Rect<T> & rect )
{
	Log::State state( append.log() );
	FLAT_UNUSED(state)

	append.log().maybeSpace();
	append.log() << Log::NoSpace();

	append.log() << "Rect(" << rect.x() << "," << rect.y() << " " << rect.width() << "x" << rect.height() << ")";
}

}
}
