
#pragma once

#include "Global.h"
#include "Int.h"




namespace Grim { namespace Math {




template <typename T> class _RealInfo {};
template <typename T> class _Real;
template <typename T> class _RealTypeForInteger {};
template <int Size> class _RealTypeForSize {};

template <typename A, typename B>
class _RealTypeForMax
{
public:
	static constexpr int max()
	{
		return qMax(
				sizeof(typename _RealInfo<typename _RealTypeForInteger<A>::Type>::HiType),
				sizeof(typename _RealInfo<typename _RealTypeForInteger<B>::Type>::HiType));
	}

	typedef typename _RealTypeForSize<max()>::Type RealType;
	typedef _Real<RealType> Type;
};

template <typename R, typename I>
class _RealValidateIntType
{
public:
	typedef _Real<R> Type;
};




#define GRIM_MATH_REAL_DEFINE_UNARY_OPERATOR(OP) \
	template <typename K> inline _Real<T> & operator OP( const _Real<K> & other ) \
	{ \
		static_assert(sizeof(typename _RealInfo<K>::Type) < sizeof(Type), \
				"Argument Real size must be less than target Real size"); \
		return *this OP _Real<T>::fromValue( other.toValue() ); \
	}

#define GRIM_MATH_REAL_DECLARE_BINARY_OPERATOR(OP) \
	template <typename K, typename L> friend inline typename _RealTypeForMax<typename _RealInfo<K>::HiType, typename _RealInfo<L>::HiType>::Type operator OP( const _Real<K> & a, const _Real<L> & b ); \
	template <typename K, typename L> friend inline _Real<K> operator OP( const _Real<K> & a, const _Int<L> & b ); \
	template <typename K, typename L> friend inline _Real<K> operator OP( const _Int<L> & a, const _Real<K> & b ); \
	template <typename K, typename L> friend inline typename _RealValidateIntType<K, typename _IntTypeForInteger<L>::Type>::Type operator OP( const _Real<K> & a, const L & b ); \
	template <typename K, typename L> friend inline typename _RealValidateIntType<K, typename _IntTypeForInteger<L>::Type>::Type operator OP( const L & a, const _Real<K> & b );

#define GRIM_MATH_REAL_DEFINE_BINARY_OPERATOR(OP) \
	template <typename K, typename L> inline typename _RealTypeForMax<typename _RealInfo<K>::HiType, typename _RealInfo<L>::HiType>::Type operator OP( const _Real<K> & a, const _Real<L> & b ) \
	{ \
		typedef typename _RealTypeForMax<typename _RealInfo<K>::HiType, typename _RealInfo<L>::HiType>::Type R; \
		return R::fromValue( a.toValue() ) OP R::fromValue( b.toValue() ); \
	} \
	\
	template <typename K, typename L> inline _Real<K> operator OP( const _Real<K> & a, const _Int<L> & b ) \
	{ return a OP _Real<K>::fromInt( b ); } \
	\
	template <typename K, typename L> inline _Real<K> operator OP( const _Int<L> & a, const _Real<K> & b ) \
	{ return b OP a; } \
	\
	template <typename K, typename L> inline typename _RealValidateIntType<K, typename _IntTypeForInteger<L>::Type>::Type operator OP( const _Real<K> & a, const L & b ) \
	{ return a OP _Real<K>::fromInt( _Real<K>::IntType::fromValue( b ) ); } \
	\
	template <typename K, typename L> inline typename _RealValidateIntType<K, typename _IntTypeForInteger<L>::Type>::Type operator OP( const L & a, const _Real<K> & b ) \
	{ return b OP _Real<K>::fromInt( _Real<K>::IntType::fromValue( a ) ); }

template <typename T>
class _Real
{
public:
	typedef typename _RealInfo<T>::Type Type;
	typedef typename _RealInfo<T>::HiType HiType;
	typedef typename _RealInfo<T>::LowType LowType;
	typedef typename _RealInfo<T>::MaskType MaskType;
	typedef _Int<typename _IntTypeForInteger<HiType>::Type> IntType;

	static const int kSize = sizeof(HiType) * 8;
	static const Type kFactor = static_cast<Type>( 1 ) << kSize;
	static const MaskType kHiMask = _RealInfo<T>::kHiMask;
	static const MaskType kLowMask = _RealInfo<T>::kLowMask;

	static constexpr _Real<T> one();

	constexpr _Real();
	constexpr _Real( const _Real<T> & other );
	template <typename K> constexpr _Real( const _Real<K> & other );

	_Real<T> & operator=( const _Real<T> & other );
	template <typename K> _Real<T> & operator=( const _Real<K> & other );

	static constexpr _Real<T> fromInt( const IntType & value );
	constexpr IntType toInt() const;

	constexpr IntType hi() const;
	constexpr IntType low() const;
	constexpr IntType round() const;

	static constexpr _Real<T> fromValue( const Type & value );
	constexpr Type toValue() const;

	// for debugging purposes
	constexpr typename _RealInfo<T>::FloatType toFloat() const;

	_Real<T> & operator+=( const _Real<T> & a );
	_Real<T> & operator-=( const _Real<T> & a );
	_Real<T> & operator*=( const _Real<T> & a );
	_Real<T> & operator/=( const _Real<T> & a );
	_Real<T> & operator%=( const _Real<T> & a );

	template <typename K> friend inline _Real<K> operator-( const _Real<K> & a );

	template <typename K> friend inline _Real<K> operator+( const _Real<K> & a, const _Real<K> & b );
	template <typename K> friend inline _Real<K> operator-( const _Real<K> & a, const _Real<K> & b );
	template <typename K> friend inline _Real<K> operator*( const _Real<K> & a, const _Real<K> & b );
	template <typename K> friend inline _Real<K> operator/( const _Real<K> & a, const _Real<K> & b );
	template <typename K> friend inline _Real<K> operator%( const _Real<K> & a, const _Real<K> & b );

	GRIM_MATH_REAL_DEFINE_UNARY_OPERATOR(+=)
	GRIM_MATH_REAL_DEFINE_UNARY_OPERATOR(-=)
	GRIM_MATH_REAL_DEFINE_UNARY_OPERATOR(*=)
	GRIM_MATH_REAL_DEFINE_UNARY_OPERATOR(/=)
	GRIM_MATH_REAL_DEFINE_UNARY_OPERATOR(%=)

	GRIM_MATH_REAL_DECLARE_BINARY_OPERATOR(+)
	GRIM_MATH_REAL_DECLARE_BINARY_OPERATOR(-)
	GRIM_MATH_REAL_DECLARE_BINARY_OPERATOR(*)
	GRIM_MATH_REAL_DECLARE_BINARY_OPERATOR(/)
	GRIM_MATH_REAL_DECLARE_BINARY_OPERATOR(%)

private:
	explicit constexpr _Real( const Type & value );

private:
	Type value_;
};




class _RealType32;

template <>
class _RealInfo<_RealType32>
{
public:
	typedef qint64 Type;
	typedef qint32 HiType;
	typedef quint32 LowType;
	typedef quint64 MaskType;
	typedef double FloatType;

	static const MaskType kHiMask = quint64(0xffffffff) << 32;
	static const MaskType kLowMask = quint64(0xffffffff);

	static const qint64 kOne = 0x100000000L;
	static const qint64 kHalfOfOne = 0x80000000L;
};

template <>
class _RealTypeForInteger<qint32>
{
public:
	typedef _RealType32 Type;
};

template <>
class _RealTypeForSize<4>
{
public:
	typedef _RealType32 Type;
};

typedef _Real<_RealType32> Real32;




template <typename T>
constexpr _Real<T> _Real<T>::one()
{ return _Real<T>::fromInt( IntType::one() ); }

template <typename T>
constexpr _Real<T>::_Real() :
	value_( 0 )
{}

template <typename T>
constexpr _Real<T>::_Real( const _Real<T> & other ) :
	value_( other.value_ )
{}

template <typename T>
constexpr _Real<T>::_Real( const Type & value ) :
	value_( value )
{}

template <typename T>
inline _Real<T> & _Real<T>::operator=( const _Real<T> & other )
{ value_ = other.value_; return *this; }

template <typename T>
inline constexpr _Real<T> _Real<T>::fromInt( const IntType & value )
{
	static_assert(sizeof(typename _RealInfo<T>::HiType) >= sizeof(typename IntType::Type),
			"Argument Int size must be less or equal than target Real HiType size");
	return _Real<T>::fromValue( static_cast<Type>( value.toValue() ) << kSize );
}

template <typename T>
inline constexpr typename _Real<T>::IntType _Real<T>::toInt() const
{ return hi(); }

template <typename T>
inline constexpr typename _Real<T>::IntType _Real<T>::hi() const
{ return IntType::fromValue( static_cast<HiType>( value_ >> kSize ) ); }

template <typename T>
inline constexpr typename _Real<T>::IntType _Real<T>::low() const
{ return IntType::fromValue( value_ & kLowMask ); }

template <typename T>
inline constexpr typename _Real<T>::IntType _Real<T>::round() const
{
	return IntType::fromValue( value_ >= 0 ?
			(value_ + _RealInfo<T>::kHalfOfOne) >> kSize :
			(value_ + _RealInfo<T>::kHalfOfOne - 1) >> kSize );
}

template <typename T>
constexpr _Real<T> _Real<T>::fromValue( const Type & value )
{ return _Real<T>( value ); }

template <typename T>
inline constexpr typename _RealInfo<T>::Type _Real<T>::toValue() const
{ return value_; }

template <typename T>
constexpr typename _RealInfo<T>::FloatType _Real<T>::toFloat() const
{ return static_cast<typename _RealInfo<T>::FloatType>( value_ ) / kFactor; }

template <typename T>
inline _Real<T> & _Real<T>::operator+=( const _Real<T> & a )
{ value_ = add_int( value_, a.value_ ); return *this; }

template <typename T>
inline _Real<T> & _Real<T>::operator-=( const _Real<T> & a )
{ value_ = sub_int( value_, a.value_ ); return *this; }

template <typename T>
inline _Real<T> & _Real<T>::operator*=( const _Real<T> & a )
{ value_ = mul_real( value_, a.value_ ); return *this; }

template <typename T>
inline _Real<T> & _Real<T>::operator/=( const _Real<T> & a )
{ value_ = div_real( value_, a.value_ ); return *this; }

template <typename T>
inline _Real<T> operator+( const _Real<T> & a, const _Real<T> & b )
{ return _Real<T>::fromValue( add_int( a.value_, b.value_ ) ); }

template <typename T>
inline _Real<T> operator-( const _Real<T> & a, const _Real<T> & b )
{ return _Real<T>::fromValue( sub_int( a.value_, b.value_ ) ); }

template <typename T>
inline _Real<T> operator*( const _Real<T> & a, const _Real<T> & b )
{ return _Real<T>::fromValue( mul_real( a.value_, b.value_ ) ); }

template <typename T>
inline _Real<T> operator/( const _Real<T> & a, const _Real<T> & b )
{ return _Real<T>::fromValue( div_real( a.value_, b.value_ ) ); }

template <typename T>
inline _Real<T> operator-( const _Real<T> & a )
{ return _Real<T>::fromValue( minus_int( a.value_ ) ); }

GRIM_MATH_REAL_DEFINE_BINARY_OPERATOR(+)
GRIM_MATH_REAL_DEFINE_BINARY_OPERATOR(-)
GRIM_MATH_REAL_DEFINE_BINARY_OPERATOR(*)
GRIM_MATH_REAL_DEFINE_BINARY_OPERATOR(/)
GRIM_MATH_REAL_DEFINE_BINARY_OPERATOR(%)




#undef GRIM_MATH_REAL_DEFINE_UNARY_OPERATOR
#undef GRIM_MATH_REAL_DECLARE_BINARY_OPERATOR
#undef GRIM_MATH_REAL_DEFINE_BINARY_OPERATOR




static constexpr Real32 kPi = Real32::fromValue( 13493037704L );
static constexpr Real32 kE  = Real32::fromValue( 11674931554L );




class _RealHelper
{
private:
	static constexpr int _calculateStringSize( const char * str, int size = 0 )
	{ return str[0] == 0 ? size : _calculateStringSize( str+1, size+1 ); }

	template <typename T>
	static constexpr T _calculateRealHi( const char * str, T value = 0 )
	{
		return (str[0] == '.' || str[0] == 0) ? value :
				(str[0] >= '0' && str[0] <= '9') ? _calculateRealHi<T>( str+1, value*10 + T(str[0] - '0') ) :
				throw "ERROR: String does not conform to real literal";
	}

	template <typename T>
	static constexpr T _calculateRealLo( const char * str, T value = 0, bool dot = false )
	{
		return str[0] == 0 ? value :
				str[0] == '.' ? (!dot ? _calculateRealLo<T>( str+1, T(0), true ) : throw "ERROR: String does not conform to real literal") :
				!dot ? _calculateRealLo<T>( str+1 ) :
				str[0] >= '0' && str[0] <= '9' ? _calculateRealLo<T>( str+1, value*10 + T(str[0] - '0'), true ) :
				throw "ERROR: String does not conform to real literal";
	}

	template <typename T>
	static constexpr int _calculatePow10( T value, int pow = 0 )
	{  return value == 0 ? pow : _calculatePow10<T>( value/10, pow+1 ); }

	template <typename T>
	static constexpr T _calculateExp10( int pow, T exp = 1 )
	{ return pow == 0 ? exp : _calculateExp10<T>( pow-1, exp*10 ); }

	static constexpr qint64 _calculateRealLoFrac32( quint32 lo, int pow )
	{ return pow == 0 ? 0 : (qint64(lo) << 32) / _calculateExp10<qint64>( pow ); }

	static constexpr qint64 _sumReal32Value( qint32 hi, quint32 lo )
	{
		return hi >= 0 ?
				(qint64(hi) << 32) + _calculateRealLoFrac32( lo, _calculatePow10( lo ) ) :
				(qint64(hi) << 32) - _calculateRealLoFrac32( lo, _calculatePow10( lo ) );
	}

public:
	static constexpr Real32 fromLiteral( const char * str )
	{
		return Real32::fromValue( _sumReal32Value(
				_calculateRealHi<qint32>( str ),
				_calculateRealLo<quint32>( str ) ) );
	}
};




}} // Grim::Math




inline constexpr Grim::Math::Real32 operator "" _real( const char * str )
{ return Grim::Math::_RealHelper::fromLiteral( str ); }




namespace Grim { namespace Debug {

template <typename T>
inline void operator<<( Log::Append & append, const Math::_Real<T> & real )
{
	append.log().append( real.toFloat() );
}

}} // Grim::Debug
