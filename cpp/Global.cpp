
#include "Global.hpp"

#include "Debug.hpp"




namespace Flat {
namespace Math {




template <>
int32_t add_int_safe<int32_t>( const int32_t a, const int32_t b )
{
	FLAT_MATH_CHECK( ((a^b) | (((a^(~(a^b) & kInt32Min)) + b)^b)) < 0 );

	return a + b;
}


template <>
int32_t sub_int_safe<int32_t>( const int32_t a, const int32_t b )
{
	FLAT_MATH_CHECK( ((a^b) & (((a ^ ((a^b) & (int32_t(1) << (sizeof(int32_t)*8-1))))-b)^b)) >= 0 );

	return a - b;
}


template <>
void inc_int_safe<int32_t>( int32_t & a )
{
	FLAT_MATH_CHECK( a < kInt32Max );

	a++;
}


template <>
void dec_int_safe<int32_t>( int32_t & a )
{
	FLAT_MATH_CHECK( a > kInt32Min );

	a--;
}


template <>
int32_t minus_int_safe<int32_t>( const int32_t a )
{
	FLAT_MATH_CHECK( a != kInt32Min );

	return -a;
}


template <>
int32_t mul_int_safe<int32_t>( const int32_t a, const int32_t b )
{
	const int64_t result = int64_t(a) * int64_t(b);
	FLAT_MATH_CHECK( result <= int64_t(kInt32Max) && result >= int64_t(kInt32Min) );

	return static_cast<int32_t>( result );
}


template <>
int32_t div_int_safe<int32_t>( const int32_t a, const int32_t b )
{
	FLAT_MATH_CHECK( b != 0 && (a != kInt32Min || b != -1) );

	return a / b;
}


template <>
int32_t mod_int_safe<int32_t>( const int32_t a, const int32_t b )
{
	FLAT_MATH_CHECK( b != 0 && (a != kInt32Min || b != -1) );

	return a % b;
}




template <>
int64_t add_int_safe<int64_t>( const int64_t a, const int64_t b )
{
	FLAT_MATH_CHECK( ((a^b) | (((a^(~(a^b) & kInt64Min)) + b)^b)) < 0 );

	return a + b;
}


template <>
int64_t sub_int_safe<int64_t>(const int64_t a, const int64_t b)
{
	FLAT_MATH_CHECK( ((a^b) & (((a ^ ((a^b) & (int64_t(1) << (sizeof(int64_t)*8-1))))-b)^b)) >= 0 );

	return a - b;
}


template <>
void inc_int_safe<int64_t>( int64_t & a )
{
	FLAT_MATH_CHECK( a < kInt64Max );

	a++;
}


template <>
void dec_int_safe<int64_t>( int64_t & a )
{
	FLAT_MATH_CHECK( a > kInt64Min );

	a--;
}


template <>
int64_t minus_int_safe<int64_t>( const int64_t a )
{
	FLAT_MATH_CHECK( a != kInt64Min );

	return -a;
}


template <>
int64_t mul_int_safe<int64_t>( const int64_t a, const int64_t b )
{
	if ( a > 0 )
	{
		if ( b > 0 )
		{
			FLAT_MATH_CHECK( a <= kInt64Max/b );
		}
		else
		{
			FLAT_MATH_CHECK( b >= kInt64Min/a );
		}
	}
	else
	{
		if ( b > 0 )
		{
			FLAT_MATH_CHECK( a >= kInt64Min/b );
		}
		else
		{
			FLAT_MATH_CHECK( (a == 0) || (b >= kInt64Max/a) );
		}
	}

	return a*b;
}


template <>
int64_t div_int_safe<int64_t>( const int64_t a, const int64_t b )
{
	FLAT_MATH_CHECK( b != 0 && (a != kInt64Min || b != -1) );

	return a / b;
}


template <>
int64_t mod_int_safe<int64_t>( const int64_t a, const int64_t b )
{
	FLAT_MATH_CHECK( b != 0 && (a != kInt64Min || b != -1) );

	return a % b;
}


int32_t mul_real_safe_32( const int32_t a, const int32_t b )
{
	int64_t product = static_cast<int64_t>(a) * b;

	// The upper 17 bits should all be the same (the sign).
	uint32_t upper = (product >> 47);

	if ( product < 0 )
	{
		FLAT_MATH_CHECK( !~upper );

#ifdef GRIM_MATH_USE_REAL_ROUNDING
		// This adjustment is required in order to round -1/2 correctly
		product--;
#endif
	}
	else
	{
		FLAT_MATH_CHECK( !upper );
	}

#ifndef GRIM_MATH_USE_REAL_ROUNDING
	return product >> 16;
#else
	int32_t result = product >> 16;
	result += (product & 0x8000) >> 15;

	return result;
#endif
}


int64_t mul_real_safe_64( const int64_t a, const int64_t b )
{
	// Each argument is divided to 32-bit parts.
	//          AB
	//      *   CD
	// -----------
	//          BD  32 * 32 -> 64 bit products
	//         CB
	//         AD
	//        AC
	//       |----| 128 bit product
	int64_t  A = (a >> 32);
	int64_t  C = (b >> 32);
	uint64_t B = (a & 0xFFFFFFFF);
	uint64_t D = (b & 0xFFFFFFFF);

	int64_t AC = A*C;
	int64_t AD_CB = A*D + C*B;
	uint64_t BD = B*D;

	int64_t product_hi = AC + (AD_CB >> 32);

	// Handle carry from lower 64 bits to upper part of result.
	uint64_t ad_cb_temp = AD_CB << 32;
	uint64_t product_lo = BD + ad_cb_temp;
	if ( product_lo < BD )
		product_hi++;

	FLAT_MATH_CHECK( product_hi >> 63 == product_hi >> 31 );

#ifndef GRIM_MATH_USE_REAL_ROUNDING
	return (product_hi << 32) | (product_lo >> 32);
#else
	// Subtracting 0x80000000 (= 0.5) and then using signed right shift
	// achieves proper rounding to result-1, except in the corner
	// case of negative numbers and lowest word = 0x80000000.
	// To handle that, we also have to subtract 1 for negative numbers.
	uint64_t product_lo_tmp = product_lo;
	product_lo -= 0x80000000;
	product_lo -= static_cast<uint64_t>(product_hi) >> 63;
	if ( product_lo > product_lo_tmp )
		product_hi--;

	// Discard the lowest 32 bits. Note that this is not exactly the same
	// as dividing by 0x100000000. For example if product = -1, result will
	// also be -1 and not 0. This is compensated by adding +1 to the result
	// and compensating this in turn in the rounding above.
	int64_t result = (product_hi << 32) | (product_lo >> 32);
	result += 1;
	return result;
#endif




#if 0
	const int64_t ah = a >> 32;
	const int64_t bh = b >> 32;

	const uint64_t al = a & 0xffffffff;
	const uint64_t bl = b & 0xffffffff;

	const int64_t h = ah * bh;
	const int64_t m = ah*bl + bh*al;
	const uint64_t l = al * bl;

	int64_t product_hi = h + (m >> 32);

	// Handle carry from lower 64 bits to upper part of result.
	const uint64_t m_temp = m << 32;
	uint64_t product_lo = l + m_temp;
	if ( product_lo < l )
		product_hi++;

	// The upper 33 bits should all be the same (the sign).
	FLAT_MATH_CHECK( (product_hi >> 63) == (product_hi >> 31) );

#ifndef GRIM_MATH_USE_REAL_ROUNDING
	return (product_hi << 32) | (product_lo >> 32);
#else

	// Subtracting 0x80000000 (= 0.5) and then using signed right shift
	// achieves proper rounding to result-1, except in the corner
	// case of negative numbers and lowest word = 0x80000000.
	// To handle that, we also have to subtract 1 for negative numbers.
	const uint64_t product_lo_tmp = product_lo;
	product_lo -= 0x80000000;
	product_lo -= (uint64_t)product_hi >> 63;
	if ( product_lo > product_lo_tmp )
		product_hi--;

	// Discard the lowest 16 bits. Note that this is not exactly the same
	// as dividing by 0x10000. For example if product = -1, result will
	// also be -1 and not 0. This is compensated by adding +1 to the result
	// and compensating this in turn in the rounding above.
	int64_t result = (product_hi << 32) | (product_lo >> 32);
	result += 1;

	return result;
#endif
#endif
}




}}
