
#pragma once

#include "Global.h"
#include "Int.h"
#include "Real.h"




namespace Grim {
namespace Math {




template <typename T>
class _Size
{
public:
	_Size();
	_Size( const T & width, const T & height );

	const T & width() const;
	const T & height() const;

	bool operator==( const _Size<T> & other ) const;
	bool operator!=( const _Size<T> & other ) const;

private:
	T width_;
	T height_;
};




typedef _Size<Int32> Size32;
typedef _Size<Int64> Size64;

typedef _Size<Real32> RealSize32;




template <typename T>
inline _Size<T>::_Size()
{}

template <typename T>
inline _Size<T>::_Size( const T & width, const T & height ) :
	width_( width ),
	height_( height )
{}

template <typename T>
inline const T & _Size<T>::width() const
{ return width_; }

template <typename T>
inline const T & _Size<T>::height() const
{ return height_; }

template <typename T>
inline bool _Size<T>::operator==( const _Size<T> & other ) const
{ return width_ == other.width_ && height_ == other.height_; }

template <typename T>
inline bool _Size<T>::operator!=( const _Size<T> & other ) const
{ return width_ != other.width_ || height_ != other.height_; }




} // namespace Math
} // namespace Grim




namespace Grim {
namespace Debug {

template <typename T>
inline void operator<<( Log::Append & append, const Grim::Math::_Size<T> & size )
{
	Log::State state( append.log() );
	FLAT_UNUSED(state)

	append.log().maybeSpace();
	append.log() << Log::NoSpace();

	append.log() << "Size(" << size.width() << "x" << size.height() << ")";
}

} // namespace Debug
} // namespace Grim
