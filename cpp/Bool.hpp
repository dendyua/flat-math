
#pragma once

#include <Flat/Debug/Log.h>

#include "Global.hpp"




namespace Flat { namespace Math {




class Bool
{
public:
	constexpr Bool() noexcept;
	constexpr explicit Bool(bool value) noexcept;

	constexpr bool value() const noexcept;

	constexpr operator bool() const noexcept;

	constexpr bool operator==(const Bool & other) const noexcept;
	constexpr bool operator!=(const Bool & other) const noexcept;

	constexpr Bool operator=(const Bool & other) noexcept;

private:
	bool value_;
};




inline constexpr Bool::Bool() noexcept :
	value_(false)
{}

inline constexpr Bool::Bool(const bool value) noexcept :
	value_(value == true)
{}

inline constexpr bool Bool::value() const noexcept
{ return value_; }

inline constexpr Bool::operator bool() const noexcept
{ return value_; }

inline constexpr bool Bool::operator==(const Bool & other) const noexcept
{ return value_ == other.value_; }

inline constexpr bool Bool::operator!=(const Bool & other) const noexcept
{ return value_ != other.value_; }

inline constexpr Bool Bool::operator=(const Bool & other) noexcept
{ value_ = other.value_; return *this; }




}}




namespace Flat { namespace Debug {

template <>
inline void Log::append<Math::Bool>(const Math::Bool & value) noexcept
{
	append(value.value());
}

}}
