
#include "Test.h"

#include <grim/math/Int.h>
#include <grim/math/Real.h>




using namespace Grim::Math;




void TestGrimMath::testAddInt32()
{
	Int32 a = Int32::fromValue( -135 );
	a += Int32::fromValue( 567 );
	QVERIFY( a.toValue() == 432 );
}


void TestGrimMath::testAddInt32Overflow()
{
	bool failed;

	// positive overflow
	failed = false;
	try
	{
		Int32 a = Int32::fromValue( kInt32Max - 5 );
		a + Int32::fromValue( 6 );
	}
	catch ( CheckException e )
	{
		failed = true;
	}
	QVERIFY( failed );

	// negative overflow
	failed = false;
	try
	{
		Int32 a = Int32::fromValue( 5 );
		a += Int32::fromValue( -6 );
		a += Int32::fromValue( kInt32Min );
	}
	catch ( CheckException e )
	{
		failed = true;
	}
	QVERIFY( failed );
}


void TestGrimMath::testSubInt32()
{
	Int32 a = Int32::fromValue( 77 );
	Int32 b = Int32::fromValue( 33 );
	QVERIFY( (a - b).toValue() == 44 );
	QVERIFY( (b - a).toValue() == -44 );
}


void TestGrimMath::testSubInt32Overflow()
{
	bool failed;

	// positive overflow
	failed = false;
	try
	{
		Int32 a = Int32::fromValue( kInt32Max - 5 );
		a -= Int32::fromValue( -6 );
	}
	catch ( CheckException e )
	{
		failed = true;
	}
	QVERIFY( failed );

	// negative overflow
	failed = false;
	try
	{
		Int32 a = Int32::fromValue( 5 );
		a -= Int32::fromValue( 6 );
		a -= Int32::fromValue( kInt32Max );
		a -= Int32::fromValue( 1 );
	}
	catch ( CheckException e )
	{
		failed = true;
	}
	QVERIFY( failed );
}


void TestGrimMath::testIncInt32()
{
	Int32 a;

	a = Int32::fromValue( -3746598 );
	a++;
	QVERIFY( a.toValue() == -3746597 );

	a = Int32::fromValue( 55934875 );
	a++;
	QVERIFY( a.toValue() == 55934876 );
}


void TestGrimMath::testIntInt32Overflow()
{
	Int32 a = Int32::fromValue( kInt32Max - 1 );
	a++;
	bool failed = false;
	try
	{
		a++;
	}
	catch ( CheckException e )
	{
		failed = true;
	}
	QVERIFY( failed );
}


void TestGrimMath::testDecInt32()
{
	Int32 a;

	a = Int32::fromValue( -3746598 );
	a--;
	QVERIFY( a.toValue() == -3746599 );

	a = Int32::fromValue( 55934875 );
	a--;
	QVERIFY( a.toValue() == 55934874 );
}


void TestGrimMath::testDecInt32Overflow()
{
	Int32 a = Int32::fromValue( kInt32Min + 1 );
	a--;
	bool failed = false;
	try
	{
		a--;
	}
	catch ( CheckException e )
	{
		failed = true;
	}
	QVERIFY( failed );
}


void TestGrimMath::testMulInt32()
{
	Int32 a = Int32::fromValue( 63 );
	Int32 b = Int32::fromValue( -137 );
	QVERIFY( (a*b).toValue() == -8631 );

	a = Int32::fromValue( -27645 );
	b = Int32::fromValue( -8761 );
	QVERIFY( (a*b).toValue() == 242197845 );
}


void TestGrimMath::testMulInt32Overflow()
{
	bool failed;

	// positive overflow
	failed = false;
	try
	{
		Int32 a = Int32::fromValue( -8764523 );
		Int32 b = Int32::fromValue( -54592713 );
		Int32 c = a*b;
	}
	catch ( CheckException e )
	{
		failed = true;
	}
	QVERIFY( failed );

	// negative overflow
	failed = false;
	try
	{
		Int32 a = Int32::fromValue( -1352387563 );
		Int32 b = Int32::fromValue( 5 );
		Int32 c = a*b;
	}
	catch ( CheckException e )
	{
		failed = true;
	}
	QVERIFY( failed );
}


void TestGrimMath::testDivInt32()
{
	Int32 a = Int32::fromValue( 5 );
	a /= Int32::fromValue( 2 );
	QVERIFY( a.toValue() == 2 );

	Int32 b = Int32::fromValue( -5 );
	b /= Int32::fromValue( 2 );
	QVERIFY( b.toValue() == -2 );
}


void TestGrimMath::testDivInt32Overflow()
{
	bool failed;

	// division by zero
	failed = false;
	try
	{
		Int32 a = Int32::fromValue( 1 );
		a /= Int32::fromValue( 0 );
	}
	catch ( CheckException e )
	{
		failed = true;
	}
	QVERIFY( failed );

	// positive overflow
	failed = false;
	try
	{
		Int32 a = Int32::fromValue( kInt32Min );
		a /= Int32::fromValue( -1 );
	}
	catch ( CheckException e )
	{
		failed = true;
	}
	QVERIFY( failed );
}


void TestGrimMath::testMulInt64()
{
	Int64 a, b, c;

	a = Int64::fromValue( 3 );
	b = Int64::fromValue( 5 );
	c = a*b;
	QVERIFY( c.toValue() == 15 );

	a = Int64::fromValue( -98374625475832L );
	b = Int64::fromValue( 5737L );
	c = a*b;
	QVERIFY( c.toValue() == -0x7D5104736E951B8L );
}


void TestGrimMath::testMulInt64Overflow()
{
	bool failed;

	// both greater than uint32 max
	failed = false;
	try
	{
		Int64 a = Int64::fromValue( 5587436011L );
		Int64 b = Int64::fromValue( 7646253085L );
		Int64 c = a*b;
	}
	catch ( CheckException e )
	{
		failed = true;
	}
	QVERIFY( failed );

	failed = false;
	try
	{
		Int64 a = Int64::fromValue( -5587436011L );
		Int64 b = Int64::fromValue( 7646253085L );
		Int64 c = a*b;
	}
	catch ( CheckException e )
	{
		failed = true;
	}
	QVERIFY( failed );

	// a high 32 * b low 32 overflow
	failed = false;
	try
	{
		Int64 a = Int64::fromValue( 56587436011L );
		Int64 b = Int64::fromValue( 646253085L );
		Int64 c = a*b;
	}
	catch ( CheckException e )
	{
		failed = true;
	}
	QVERIFY( failed );

	failed = false;
	try
	{
		Int64 a = Int64::fromValue( 56587436011L );
		Int64 b = Int64::fromValue( -646253085L );
		Int64 c = a*b;
	}
	catch ( CheckException e )
	{
		failed = true;
	}
	QVERIFY( failed );
}


void TestGrimMath::testMulReal32()
{
#if 0
	Real32 a, b, c;

	a = Real32::fromFloat( 5 );
	b = Real32::fromFloat( 3 );
	c = a * b;
	qDebug() << c << c.toFloat();

	a = Real32::fromFloat( 736455.7658 );
	b = Real32::fromFloat( 0.007462847 );
	c = a * b;
	qDebug() << c << c.toFloat();
#endif
}


void TestGrimMath::testRoundReal32()
{
	// zero round
	QVERIFY( Real32::fromValue( 0 ).round() == 0 );

	// positive round
	QVERIFY( Real32::fromValue( (qint64(1) << 32) + 0x00000000 ).round() == 1 );
	QVERIFY( Real32::fromValue( (qint64(1) << 32) + 0x00000001 ).round() == 1 );
	QVERIFY( Real32::fromValue( (qint64(1) << 32) + 0x7fffffff ).round() == 1 );
	QVERIFY( Real32::fromValue( (qint64(1) << 32) + 0x80000000 ).round() == 2 );

	// negative round
	QVERIFY( Real32::fromValue( (qint64(-1) << 32) - 0x00000000 ).round() == -1 );
	QVERIFY( Real32::fromValue( (qint64(-1) << 32) - 0x00000001 ).round() == -1 );
	QVERIFY( Real32::fromValue( (qint64(-1) << 32) - 0x7fffffff ).round() == -1 );
	QVERIFY( Real32::fromValue( (qint64(-1) << 32) - 0x80000000 ).round() == -2 );
	QVERIFY( Real32::fromValue( (qint64(-1) << 32) - 0x80000001 ).round() == -2 );
}


inline bool compareFloat( float a, float b, float t )
{
	return (a >= b - t) && (a <= b + t);
}


void TestGrimMath::testReal32Literals()
{
	// zero
	QVERIFY( 0_real .toValue() == 0 );

	// integer
	QVERIFY( 767_real .toValue() == qint64(767) << 32 );
	QVERIFY( -919_real .toValue() == qint64(-919) << 32 );

	// frac
	QVERIFY( .1_real .toValue() == (qint64(1) << 32) / 10 );
	QVERIFY( -.333_real .toValue() == -(qint64(333) << 32) / 1000 );

	// real
	QVERIFY( compareFloat( 157.765_real .toFloat(), 157.765f, 0.0001f ) );
	QVERIFY( compareFloat( -157.765_real .toFloat(), -157.765f, 0.0001f ) );
}




QTEST_MAIN( TestGrimMath )
